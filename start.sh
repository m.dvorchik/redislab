cd redis-cluster
docker-compose up -d
cd ../src
pip install --no-cache-dir -r requirements.txt
python3 main.py
