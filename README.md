# Redis master-slave
## Дворчик 1 задание:
### Настроить репликацию для Redis. Показать сценарий, при котором после останова Master, клиенты могут подключиться к Slave и работать с сохранившимися данными.
**Используется Docker Compose** 
### Автоматическая установка необходимых модулей и запуск окружения через скрипт из корня проекта: 
    ./start.sh
### Ручной запуск: 
* В папке redis-cluster запускаем кластер:<br>
    <code>docker-compose up -d</code>
* Установка зависимостей для python3(перечислены в requirements.txt):<br>
    <code>pip install --no-cache-dir -r ../src/requirements.txt</code>
* Затем скрипт:<br>
    <code>python3 ../src/main.py</code>


