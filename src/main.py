import redis
import subprocess


def try_connect(redis_server):
    is_connect = True
    try:
        response = redis_server.client_list()
    except redis.ConnectionError:
        is_connect = False
    finally:
        return is_connect

def report_connect(master, slave):
    if (try_connect(master) == True):
        print ("master is connect: True")
    if (try_connect(slave) == True):
        print ("slave is connect: True")
    if (try_connect(master) == False):
        print ("master is connect: False")
    if (try_connect(slave) == False):
        print ("slave is connect: False")

def set_key(redis, role, key, value):
    if (try_connect(redis) == True):
        redis.set(key, value)
        print(role + ": set key:"+key+" value:"+value.__str__())
def get_key(redis, role, key):
    if (try_connect(redis) == True):
        value = redis.get(key).decode("UTF-8")
        print(role + ": get key:"+key+" value:"+value.__str__())

master = redis.Redis(host='127.0.0.1', port='10101')
slave = redis.Redis(host='127.0.0.1', port='10102')

report_connect(master, slave)

for i in range(9):
    set_key(master, "master", "k"+i.__str__(), i)
print()
for i in range(9):
    get_key(master, "master", "k"+i.__str__())

print("Stopping master...")   
subprocess.check_output('docker stop redis-cluster_master_1; exit 0', shell=True)
print()
report_connect(master, slave)
print()
print("Getting old data from slave:")
for i in range(9):
    get_key(slave, "slave", "k"+i.__str__())
print("Starting master again...")
subprocess.check_output('docker start redis-cluster_master_1; exit 0', shell=True)
print("Master started")
    